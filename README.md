# Roteiro 07
### Tipos Abstratos de Dados (Fila e Pilha)

- [Stack](https://gitlab.com/ufcg-archieve/leda/r07/-/blob/956f50887999681b9ccadeeeb738965ea77fcd38/src/main/java/adt/stack/StackImpl.java)
- [Queue](https://gitlab.com/ufcg-archieve/leda/r07/-/blob/956f50887999681b9ccadeeeb738965ea77fcd38/src/main/java/adt/queue/QueueImpl.java)
- [CircularQueue](https://gitlab.com/ufcg-archieve/leda/r07/-/blob/956f50887999681b9ccadeeeb738965ea77fcd38/src/main/java/adt/queue/CircularQueue.java)
- [QueueUsingStack](https://gitlab.com/ufcg-archieve/leda/r07/-/blob/956f50887999681b9ccadeeeb738965ea77fcd38/src/main/java/adt/queue/QueueUsingStack.java)